console.log("Hello World!");

//It will comment parts of the code that gets ignored by the language

/*
	There are two types of comments
	1. the single line comment denoted by two slashes.
	2. The multi-line comment denoted by slash and asterisk.
*/

// [Section] Syntax, statements
	// Statements in programming are instructions that we tell the compute to perform
	// JS statements usually end with semicolon (;)
	// Semicolons are not required in JS but we will use it to help us train to locate where statement ends.
	// A syntax in programming, it is the set of rules that we decribes statements must be constructed.
	// All lines/blocks of code should be written in specific manner. This is due to how these codes where initially programmed to fuction in a certain manner.

// [Section] Variable
	// Variables are used to contain data
	// Any information that is used by an application is stored in what we call the memory
	// When we create variables, certain portions of device's memory is given a name that we call variables.
	// This makes it easier for us to associate information stored in our devices to actual "names" information

// Declaring Variables
	// It tells our devices that a variable name is created and is ready to store data.
	// Syntax
		/*let/const variableName;*/

let myVariable = "Ada Lovelace"
let variable;
console.log(variable);
// Const keyword is use when the value of the variable won't change. 
const constVariable = "John Doe"
// console.log() is useful for printing values of variables of certain results of code into the browser's console.
console.log(myVariable);

/*
	Guides in writing Varibales
		1. Use the let keyword followed the variable name of your choose and use the assignment operator (=) to assign value.
		2. Variable names should be start with lowercase character, use camelCasing for the multiple words.
		3. For constant variables, use the 'const' keywords.
			Notes: If we use const keyword in declaring a variable, we cannot change the value of its variable.
		4. Varibale names should be indicative(descriptive) of the value being stored to avoid confusion.
*/

// Delcare and initialize
	// Initializing variables - the instance when a variable is fiven its first/initial value.
	// Syntax: 
		// let/const variableName = initial value;

// Declaration and Initialization of the variable occur
/*console.log(productName)*/
let productName = "desktop computer";
console.log(productName);


// Declaration
let desktopName;

console.log(desktopName);
// initialization of the value of variable desktopName
desktopName = "Dell"
console.log(desktopName);

// Reassigning value
productName = "Personal Computer";
console.log(productName);


// This reassignment will cause an error since we cannot change.reassign the initial value of constant variable
/*const name = "Rj"
console.log(name);

name = "Roland";
console.log(name);*/

// This will cause an error on our code because the productName variable is already taken
/*let productName = "Laptop"; */

// var vs let/const
	// some of you my wonder why we used let and const keyword in declaring a varibale when we search online, we usually see var.
	// var is also used in declaring variable but var is an ecmaScript1 feature (1997).

let lastName;
lastName = "Doroteo";
console.log(lastName);

/*
Using var, bad practice.

batch = "Batch 241"
var batch
console.log(batch)*/

////////////////////////////////

// let/const local / Global scope
/*
	Scope sessntially means where these variables are available for use.

	let/const are block scope

	A block is a chunk of code bounded by {}
*/

/*let outerVariable = "Hello";
{
	let innerVariable = "Hello Again";
	console.log(innerVariable);
}

console.log(outerVariable);*/

/*const outerVariable = "Hello";
{
	const innerVariable = "Hello again"
}
console.log(innerVariable);
console.log(outerVariable);*/

var outerVariable = "Hello";
{
	var innerVariable = "Hello again"
}
console.log(innerVariable);
console.log(outerVariable);

// Multiple variable declation

let productCode = "DC017", productBrand = "Dell";
console.log(productCode);
console.log(productBrand);


// [Section] Data Types
// Strings
/*
	Strings are series of characters that create a word, a phrase, a sentence or anthing related to creating a text.
	String in JavaScript can be written using either a single ('') or double ("") quote
*/

let country = "Philippines";
let province = 'Manila';
console.log(country);
console.log(province);

// Concatenate Strings
// Multiple string values can be combined to create a single string using the "+" symbol

let fullAddress = province + "," + country;
console.log(fullAddress);

let greeting = 'I live in the' + country;
console.log(greeting)

// Declaring a string using an escape character
let message = 'John\'s employees went home early';
console.log(message)

message = "John's employees went home early";
console.log(message);

// "\n" - referes to creating a new line in between text

let mainAddress = "Metro Manila\n\nPhilippines";
console.log(mainAddress);

// Numbers
// Integer/Whole number
let headCount = 27;
console.log(headCount);
console.log(typeof headCount);

// Decimal Number
let grade = 98.7;
console.log(grade);

// Exponential Notation
let planetDistance = 2e10;
console.log(planetDistance);

let planetDistance1 = "2e10";
console.log(planetDistance);

// Combining text and strings
console.log("John's grade last quarter is " + grade);

// Boolean
// Boolean values are normally used to store values relating to the state of certain things.
// true or false
let isMarried = false;
let inGoodConduct = true;
console.log(isMarried);
console.log(inGoodConduct);

// Arrays
// Arrays are a special kind of data type that's use to store multiple values

/*
	Syntax:
		let/const arrayName = [elementA, elementB, .....]
*/

// Similar data types
let grades = [98, 92.1, 90.1, 94.7];
console.log(grades);
console.log(typeof grades);

// different data types
let details = ["John", 32, true];
console.log(details);

// Objects
// Objects are another special kind of data type that's to mimic real world objects or items.

/*
	Syntax:
	let/const objectName = {
		propertyA: value,
		propertyB: value
	}
*/

let person = {
	firtName: "John",
	lastName: "Smith",
	age: 32,
	isMarried: false,
	contact: ["09123456789", "8123-4567"],
	address: {
		houseNumber: "345",
		city: "Manila",
	}
}
console.log(person);

let person1 = {}
console.log(person1);

/*
	Constant Objects and Arraus
*/
const anime = ["one piece", "one punch man", "your lie in April"];
console.log(anime);
anime[0] = ['kimetsu no yaba']
console.log(anime)

/*const anime1 = ["one piece", "one punch man", "your lie in April"];
console.log(animea);
anime1 = ['kimetsu no yaba']
console.log(animea)*/

// Null
// it is used to intentionally express the absence of a value in a variable declaration/initalization.
let spouse = null;
console.log(spouse);

let myNumber = 0;
let muString = "";

// Undefined
// Represent the state of a variable that has been declared but without an assigned value
let inARelationship;
console.log(inARelationship);









